from scipy.ndimage import rotate
import os
import cv2
import collections
from sklearn.svm import SVC
VIDEO_PATH = 'video03.mp4'
# cap = cv2.VideoCapture(VIDEO_PATH)

import cv2
import numpy as np
 
# Create a VideoCapture object
cap = cv2.VideoCapture(VIDEO_PATH)
 
# Check if camera opened successfully
if (cap.isOpened() == False): 
  print("Unable to read camera feed")
 
# Default resolutions of the frame are obtained.The default resolutions are system dependent.
# We convert the resolutions from float to integer.
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
print(frame_width)
print(frame_height)
# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 30, (frame_width,frame_height))
 
while(True):
  ret, frame = cap.read()
 
  if ret == True: 
    # frame = rotate(frame, -90)
    # frame = cv2.resize(frame, (600,800),interpolation=cv2.INTER_AREA)
    # Write the frame into the file 'output.avi'
    out.write(frame)
 
    # Display the resulting frame    
    cv2.imshow('frame',frame)
 
    # Press Q on keyboard to stop recording
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else:
    break 
 
# When everything done, release the video capture and video write objects
cap.release()
out.release()
 
# Closes all the frames
cv2.destroyAllWindows() 